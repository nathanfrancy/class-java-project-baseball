package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.database.MySQLConnection;
import proj.team.Team;

@WebServlet("/Init")
public class Init extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public Init() {
        super();
    }
	
	public static String getTeams() {
		String s = "";
		
		ArrayList<Team> teams = new ArrayList<Team>();
		teams = MySQLConnection.getTeams();

		for (Team t : teams) {
			s += "<option value='" + t.getId() + "'>" + t.getName() + "</option>";
		}
		
		return s;
	}
	
	public static boolean liveGame(int gameid) {
		if (MySQLConnection.isLiveGame(gameid) > 0) {
			return true;
		}
		else 
			return false;
	}

}
