package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.game.GameStats;

@WebServlet("/Double")
public class Double extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Double() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int gamestatsid = Integer.parseInt(request.getParameter("gamestatsid"));
		
		GameStats gs = new GameStats(gamestatsid);
		gs.doDouble();
		gs.commitStats();
		
		response.sendRedirect("ingame.jsp?gameid=" + gs.getGameid());
	}

}