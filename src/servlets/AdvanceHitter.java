package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.game.GameStats;

@WebServlet("/AdvanceHitter")
public class AdvanceHitter extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdvanceHitter() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// new GameStats object from passed 'gamestatsid'
		int gamestatsid = Integer.parseInt(request.getParameter("gamestatsid"));
		GameStats gs = new GameStats(gamestatsid);
		
		// advance the hitter and save it
		gs.advanceHitter();
		gs.commitStats();
		
		response.sendRedirect("ingame.jsp?gameid=" + gs.getGameid());
	}

}