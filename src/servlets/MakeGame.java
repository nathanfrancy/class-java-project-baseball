package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.database.Logger;
import proj.database.MySQLConnection;
import proj.team.Team;

@WebServlet("/makegame")
public class MakeGame extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MakeGame() {
        super();
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Team away = new Team(Integer.parseInt(request.getParameter("away-box")));
		Team home = new Team(Integer.parseInt(request.getParameter("home-box")));
		
		// make a game with the home and away team
		MySQLConnection.makeGame(home.getId(), away.getId());
		
		Logger.makegame(away.getId(), home.getId());
		
		// redirect with the latest game that was added
		response.sendRedirect("ingame.jsp?gameid=" + MySQLConnection.getLatestGame());
	}

}