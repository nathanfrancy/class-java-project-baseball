package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.game.GameStats;

@WebServlet("/Triple")
public class Triple extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Triple() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// new GameStats object based on 'gamestatsid' passed
		int gamestatsid = Integer.parseInt(request.getParameter("gamestatsid"));
		GameStats gs = new GameStats(gamestatsid);
		
		// perform a triple and save
		gs.doTriple();
		gs.commitStats();
		
		response.sendRedirect("ingame.jsp?gameid=" + gs.getGameid());
	}

}