package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.game.GameStats;

@WebServlet("/AdvanceInning")
public class AdvanceInning extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdvanceInning() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// make a new GameStats object with the passed gamestatsid
		int gamestatsid = Integer.parseInt(request.getParameter("gamestatsid"));
		GameStats gs = new GameStats(gamestatsid);
		
		// advance the inning and save
		gs.advanceInning();
		gs.commitStats();
		
		response.sendRedirect("ingame.jsp?gameid=" + gs.getGameid());
	}

}