package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import proj.game.GameStats;

@WebServlet("/HomeRun")
public class HomeRun extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
    public HomeRun() {
        super();
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// get the gamestatsid and make new GameStats object
		int gamestatsid = Integer.parseInt(request.getParameter("gamestatsid"));
		GameStats gs = new GameStats(gamestatsid);
		
		// perform a home run, then log it
		gs.doHomeRun();
		gs.commitStats();
		
		// send client back to ingame page
		response.sendRedirect("ingame.jsp?gameid=" + gs.getGameid());
	}

}
