package proj.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import proj.player.Player;
import proj.team.Team;

public class MySQLConnection {

	private static String url = "jdbc:mysql://localhost:3306/baseball_java";
	private static String driver = "com.mysql.jdbc.Driver";
	private static Connection conn = null;
	static ArrayList<Team> teams = new ArrayList<Team>();
	
	public MySQLConnection() {}
	
	/**
	 * Gets a Connection object for all other utilities
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, "root", "");
		} 
		catch (ClassNotFoundException cnfe) {
	    	cnfe.printStackTrace();
	    } catch (SQLException sqle) {
	    	sqle.printStackTrace();
	    }
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
	/**
	 * Getting all the teams in an ArrayList<Team>
	 * @return ArrayList<Team> : all teams in the database from the TEAM table
	 */
	public static ArrayList<Team> getTeams() {
		try {
			Connection conn = getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM TEAM;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                teams.add(new Team(initiateRs.getInt("team_id")));
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return teams;
	}
	
	/**
	 * Returns number of games with a specified gameid, essentially saying if it exists or not
	 * @param gameid
	 * @return : 0 or > 0 to key off of
	 */
	public static int isLiveGame(int gameid) {
		int numRows = 0;
		try {
			Connection conn = getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM GAME WHERE game_id = " + gameid + " AND game_statusid = 1;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);
            
            if (initiateRs.next()) {
            	numRows++;
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return numRows;
	}
	
	/**
	 * Inserts a new game record in the GAME table
	 * @param homeid
	 * @param awayid
	 * @return boolean : true or false, based on whether successful or not
	 */
	public static boolean makeGame(int homeid, int awayid) {
		
		try {
			Connection conn = getConnection();
			
			Team home = new Team(homeid);
			Team away = new Team(awayid);

			// insert a new row into GAME with home team and away team, and status id of 1 (currently playing)
            Statement initiateStmt = conn.createStatement();
            String initiateString = "INSERT INTO GAME (game_hometeamid, game_awayteamid, game_statusid) "
            		+ "VALUES (" + homeid + ", " + awayid + ", 1);";
            initiateStmt.executeUpdate(initiateString);
            
            Statement statStmt = conn.createStatement();
            String statString = "INSERT INTO GAMESTATS (gamestats_homehitterid, gamestats_awayhitterid, gamestats_hittingteamid, gamestats_awayteamid, gamestats_hometeamid, gamestats_hitterid, gamestats_gameid, gamestats_strikes, gamestats_balls, gamestats_outs, gamestats_homescore, gamestats_awayscore, gamestats_inning, gamestats_inningstate, gamestats_awaypitcherid, gamestats_homepitcherid, gamestats_first, gamestats_second, gamestats_third) "
            		+ "VALUES ("+ home.getFirstBatter() +", " + away.getFirstBatter() + ", "+ awayid +", "+ awayid +", "+ homeid+ ", 1, "+ getLatestGame() +", 0, 0, 0, 0, 0, 1, 'Top', "+ getPitcherId(away.getId()) +", " + getPitcherId(home.getId()) + ", 0, 0, 0);";
            statStmt.executeUpdate(statString);

            statStmt.close();
            initiateStmt.close();
            conn.close();
            return true;
            
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	/**
	 * Utility that returns the id of the latest game, for forwarding once created
	 * @return int : latest gameid
	 */
	public static int getLatestGame() {
		int latest = 0;
		
		try {
			Connection conn = getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT game_id FROM GAME ORDER BY game_id asc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                latest = initiateRs.getInt("game_id");
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return latest;
	}
	
	/**
	 * Get an ArrayList of Player objects for a specified team id
	 * Only returns batting list players, sorted by batting order
	 * @param teamid
	 * @return arraylist of players for a specific team
	 */
	public static ArrayList<Player> getTeam(int teamid) {
		ArrayList<Player> players = new ArrayList<Player>();
		try {
			Connection conn = getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM PLAYER WHERE player_teamid = " + teamid + " AND player_batorder != 0 ORDER BY player_batorder asc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                players.add(new Player(initiateRs.getInt("player_id")));
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return players;
	}
	
	public static int getFirstBatter(int teamid) {
		int batterid = 0;
		
		try {
			Connection conn = getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT player_id FROM PLAYER WHERE player_teamid = " + teamid + " AND player_batorder = 1 ORDER BY player_id asc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
            	batterid = initiateRs.getInt("player_id");
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return batterid;
	}
	
	public static void closeGame(int gameid) {

		try {
			Connection conn = (Connection) MySQLConnection.getConnection();

			// close the specified game
            Statement stateStmt = conn.createStatement();
            String statString = "UPDATE GAME SET game_statusid = 0 WHERE game_id = " + gameid;
            stateStmt.executeUpdate(statString);
            
            stateStmt.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public static int getPlayerId(int teamid, int batorder) {
		int pid = 0;
		
		try {
			Connection conn = getConnection();
			
			if ( (batorder >= 1) && (batorder <= 8) ) {
				batorder++;
			}
			else {
				batorder = 1;
			}
			
			
            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT player_id FROM PLAYER WHERE player_teamid = "+ teamid +" AND player_batorder = " + batorder + " ORDER BY player_id asc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                pid = initiateRs.getInt("player_id");
            }
            
            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return pid;
	}
	
	public static int getPitcherId(int teamid) {
		int pid = 0;
		
		try {
			Connection conn = getConnection();
			
            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT player_id FROM PLAYER WHERE player_teamid = "+ teamid +" AND player_position = 'P' ORDER BY player_id asc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                pid = initiateRs.getInt("player_id");
            }
            
            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return pid;
	}
	
	

}