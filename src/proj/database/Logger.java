package proj.database;

import proj.player.*;
import proj.team.Team;

public class Logger {
	
	public Logger() {}
	
	public static void makegame(int awayteamid, int hometeamid) {
		Team away = new Team(awayteamid);
		Team home = new Team(hometeamid);
		
		System.out.println("-- New Game Started --");
		System.out.println(away.getLocation() + " " + away.getName() + " @ " + home.getLocation() + " " + home.getName());
	}
	
	public static void strikeout(String inning, int batterid, int pitcherid) {
		Player batter = new Player(batterid);
		Player pitcher = new Player(pitcherid);
		System.out.println(inning + ": " + pitcher.getShortName() + " struck out " + batter.getShortName());
	}
	
	public static void walk(String inning, int batterid, int pitcherid) {
		Player batter = new Player(batterid);
		Player pitcher = new Player(pitcherid);
		System.out.println(inning + ": " + pitcher.getShortName() + " walked " + batter.getShortName());
	}
	
	public static void single(String inning, int batterid) {
		Player batter = new Player(batterid);
		System.out.println(inning + ": " + batter.getShortName() + " singled");
	}
	
	public static void doub(String inning, int batterid) {
		Player batter = new Player(batterid);
		System.out.println(inning + ": " + batter.getShortName() + " doubled");
	}
	
	public static void triple(String inning, int batterid) {
		Player batter = new Player(batterid);
		System.out.println(inning + ": " + batter.getShortName() + " tripled");
	}
	
	public static void homerun(String inning, int batterid) {
		Player batter = new Player(batterid);
		System.out.println(inning + ": " + batter.getShortName() + " hit a home run!");
	}
	
	public static void score(int batterid) {
		Player batter = new Player(batterid);
		System.out.println("  [resulting run]: " + batter.getShortName() + " scored!");
	}
	
	public static void out(String inning, int batterid, int outs) {
		Player batter = new Player(batterid);
		System.out.println(inning + ": " + batter.getShortName() + " hit out [" + outs + " out]");
	}
	
}