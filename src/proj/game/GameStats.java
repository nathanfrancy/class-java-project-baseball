package proj.game;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import proj.database.Logger;
import proj.database.MySQLConnection;
import proj.player.Player;

import com.mysql.jdbc.Connection;

public class GameStats extends Game {
	
	private int gamestatsid;
	private int awayteamid;
	private int hometeamid;
	private int awayscore;
	private int homescore;
	private int hitterid;
	private int homepitcherid;
	private int awaypitcherid;
	private int hittingteamid;
	private int awayhitterid;
	private int homehitterid;
	private int gameid;
	private int strikes;
	private int balls;
	private int outs;
	private int inning;
	private String inningState;
	private int firstrunnerid;
	private int secondrunnerid;
	private int thirdrunnerid;

	public GameStats(int id) {
		super(id);
		
		try {
            Connection conn = (Connection) MySQLConnection.getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM GAMESTATS WHERE gamestats_gameid = " + id + ";";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
            	this.gamestatsid = initiateRs.getInt("gamestats_id");
                this.awayteamid = initiateRs.getInt("gamestats_awayteamid");
                this.hometeamid = initiateRs.getInt("gamestats_hometeamid");
                this.homescore = initiateRs.getInt("gamestats_homescore");
                this.awayscore = initiateRs.getInt("gamestats_awayscore");
                this.hittingteamid = initiateRs.getInt("gamestats_hittingteamid");
                this.hitterid = initiateRs.getInt("gamestats_hitterid");
                this.homepitcherid = initiateRs.getInt("gamestats_homepitcherid");
                this.awaypitcherid = initiateRs.getInt("gamestats_awaypitcherid");
                this.gameid = initiateRs.getInt("gamestats_gameid");
                this.strikes = initiateRs.getInt("gamestats_strikes");
                this.balls = initiateRs.getInt("gamestats_balls");
                this.outs = initiateRs.getInt("gamestats_outs");
                this.inning = initiateRs.getInt("gamestats_inning");
                this.homehitterid = initiateRs.getInt("gamestats_homehitterid");
                this.awayhitterid = initiateRs.getInt("gamestats_awayhitterid");
                this.inningState = initiateRs.getString("gamestats_inningstate");
                this.firstrunnerid = initiateRs.getInt("gamestats_first");
                this.secondrunnerid = initiateRs.getInt("gamestats_second");
                this.thirdrunnerid = initiateRs.getInt("gamestats_third");
            }

            initiateRs.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	public void commitStats() {
		try {
			Connection conn = (Connection) MySQLConnection.getConnection();

			// update the GAMESTATS record for THIS game, and update any of the class variables, which might have been updated
            Statement stateStmt = conn.createStatement();
            String statString = "UPDATE GAMESTATS SET "
			            		+ "gamestats_inning = " + this.inning
			            		+ ", gamestats_inningstate = '" + this.inningState
			            		+ "', gamestats_awayscore = " + this.awayscore 
			            		+ ", gamestats_homescore = " + this.homescore 
			            		+ ", gamestats_hitterid = " + this.hitterid
			            		+ ", gamestats_homepitcherid = " + this.homepitcherid
			            		+ ", gamestats_awaypitcherid = " + this.awaypitcherid
			            		+ ", gamestats_strikes = " + this.strikes
			            		+ ", gamestats_balls = " + this.balls
			            		+ ", gamestats_outs = " + this.outs
			            		+ ", gamestats_hittingteamid = " + this.hittingteamid
			            		+ ", gamestats_awayhitterid = " + this.awayhitterid
			            		+ ", gamestats_homehitterid = " + this.homehitterid
			            		+ ", gamestats_first = " + this.firstrunnerid
			            		+ ", gamestats_second = " + this.secondrunnerid
			            		+ ", gamestats_third = " + this.thirdrunnerid
			            		+ " WHERE gamestats_id = " + this.gamestatsid;
            stateStmt.executeUpdate(statString);
            
            stateStmt.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public void strike() {
		
		// if there were 2 strikes, 
		if (strikes == 2) {
			
			if (inningState.equals("Top")) {
				Logger.strikeout(inningState + " " + inning, awayhitterid, homepitcherid);
			}
			else if (inningState.equals("Bottom")) {
				Logger.strikeout(inningState + " " + inning, homehitterid, awaypitcherid);
			}
			
			strikes = 0;
			this.out();
		}
		else // increment strikes by 1
			strikes++;
	}
	
	public void ball() {
		
		if (balls == 3) {
			balls = 0;
			strikes = 0;
			doWalk();
			this.advanceHitter();
		}
		else 
			balls++;
	}
	
	public void out() {
		
		// reset match-up counters
		balls = 0;
		strikes = 0;
		//resetCount();
		
		this.advanceHitter();
		
		// reset everything because the half-inning is over, then advance the inning
		if (outs == 2) {
			outs = 0;
			this.advanceInning();
		}
		else { 
			outs++;
		}
	}
	
	public void hitout() {
		
		// reset match-up counters
		balls = 0;
		strikes = 0;
		
		// Log the out in the console
		if (inningState.equals("Top")) {
			Logger.out(inningState + " " + inning, awayhitterid, outs+1);
		}
		else if (inningState.equals("Bottom")) {
			Logger.out(inningState + " " + inning, homehitterid, outs+1);
		}
		
		this.advanceHitter();
		
		// reset everything because the half-inning is over, then advance the inning
		if (outs == 2) {
			outs = 0;
			this.advanceInning();
		}
		else { 
			outs++;
		}
	}
	
	public void advanceHitter() {
		
		Player p = null;
		
		// if the away team is hitting
		if (inningState.equals("Top")) {
			p = new Player(awayhitterid);
			int batorderaway = p.getBatorder();
			awayhitterid = MySQLConnection.getPlayerId(awayteamid, batorderaway);
			this.commitStats();
		}
		
		// the home team is hitting
		else if (inningState.equals("Bottom")) {
			p = new Player(homehitterid);
			int batorderhome = p.getBatorder();
			homehitterid = MySQLConnection.getPlayerId(hometeamid, batorderhome);
			this.commitStats();
		}
		
	}
	
	public void advanceInning() {
		
		// remove baserunners
		firstrunnerid = 0;
		secondrunnerid = 0;
		thirdrunnerid = 0;
		
		// if top of 9th over, and home team is up, end the game
		if ( ((inningState.equals("Top")) && (inning >= 9)) && (homescore > awayscore) ) {
			inning = 0;
			inningState = "Game over.";
			MySQLConnection.closeGame(gameid);
		}
		
		// if bottom of the ninth and home score doesn't equal away score, end the game
		else if ( ((inningState.equals("Bottom")) && (inning >= 9)) && (homescore != awayscore) ) {
			inning = 0;
			inningState = "Game Over";
			MySQLConnection.closeGame(gameid);
		}
		
		// advance the inning
		else {
			if(inningState.equals("Bottom")) {
				inning++;
				inningState = "Top";
				hittingteamid = awayteamid;
			}
			else {
				inningState = "Bottom";
				hittingteamid = hometeamid;
			}
		}
	}
	
	public void doSingle() {
		resetCount();
		//balls = 0;
		//strikes=0;
		// get the initial runners
		int bef_first = firstrunnerid;
		int bef_second = secondrunnerid;
		int bef_third = thirdrunnerid;
		int batterid = 0;
		
		
		resetRunners();
		
		if (inningState.equals("Top")) {
			batterid = awayhitterid;
		}
		else if (inningState.equals("Bottom")) {
			batterid = homehitterid;
		}
		
		Logger.single(inningState + " " + inning, batterid);
		
		// put the batter on first
		firstrunnerid = batterid;

		// score the runner on third if he exists
		if (bef_third != 0) {
			run();
			Logger.score(bef_third);
		}

		// advance the runner on second if he exists
		if (bef_second != 0) {
			thirdrunnerid = bef_second;
		}
		
		// advance the runner on third if he exists
		if (bef_first != 0) {
			secondrunnerid = bef_first;
		}
		
		advanceHitter(); // advance the hitter (previous batter is on first now)
	}
	
	public void doDouble() {
		
		// get the 
		int bef_first = firstrunnerid;
		int bef_second = secondrunnerid;
		int bef_third = thirdrunnerid;
		int batterid = 0;
		resetCount();
		
		resetRunners();
		
		if (inningState.equals("Top")) {
			batterid = awayhitterid;
		}
		else if (inningState.equals("Bottom")) {
			batterid = homehitterid;
		}
		
		Logger.doub(inningState + " " + inning, batterid);
		
		// put the batter on second
		secondrunnerid = batterid;

		// score the runner on third if he exists
		if (bef_third != 0) {
			run();
			Logger.score(bef_third);
		}

		// score the runner on second if he exists
		if (bef_second != 0) {
			run();
			Logger.score(bef_second);
		}
		
		// advance the runner on first to third
		if (bef_first != 0) {
			thirdrunnerid = bef_first;
			
		}
		
		advanceHitter(); // advance the hitter (previous batter is on first now)
	}
	
	public void doTriple() {
		
		// get the initial baserunners before the play
		int bef_first = firstrunnerid;
		int bef_second = secondrunnerid;
		int bef_third = thirdrunnerid;
		int batterid = 0;
		
		resetCount();
		resetRunners();
		
		if (inningState.equals("Top")) {
			batterid = awayhitterid;
		}
		else if (inningState.equals("Bottom")) {
			batterid = homehitterid;
		}
		
		Logger.triple(inningState + " " + inning, batterid);
		
		// put the batter on second
		thirdrunnerid = batterid;
	
		// score the runner on third if he exists
		if (bef_third != 0) {
			run();
			Logger.score(bef_third);
		}
	
		// score the runner on second if he exists
		if (bef_second != 0) {
			run();
			Logger.score(bef_second);
		}
		
		// score the runner on first if he exists
		if (bef_first != 0) {
			run();
			Logger.score(bef_first);
		}
		
		advanceHitter(); // advance the hitter (previous batter is on first now)
	}
	
	public void doHomeRun() {
		
		// get the initial baserunners before the play
		int bef_first = firstrunnerid;
		int bef_second = secondrunnerid;
		int bef_third = thirdrunnerid;
		int batterid = 0;
		
		resetCount();
		resetRunners();
		
		if (inningState.equals("Top")) {
			batterid = awayhitterid;
		}
		else if (inningState.equals("Bottom")) {
			batterid = homehitterid;
		}
		
		Logger.homerun(inningState + " " + inning, batterid);
	
		// score the runner on third if he exists, log
		if (bef_third != 0) {
			run();
			Logger.score(bef_third);
		}
	
		// score the runner on second if he exists, log
		if (bef_second != 0) {
			run();
			Logger.score(bef_second);
		}
		
		// score the runner on first if he exists, log 
		if (bef_first != 0) {
			run();
			Logger.score(bef_first);
		}
		
		// score the hitter and log it
		run();
		Logger.score(batterid);
		
		advanceHitter(); // advance the hitter (previous batter is on first now)
	}
	
	public void run() {
		
		// the away team is scoring
		if (inningState.equals("Top")) {
			awayscore++;
		}
		
		// the home team is scoring
		if (inningState.equals("Bottom")) {
			homescore++;
		}
		
	}
	
	public void doWalk() {

		// get the initial baserunners before the play
		int bef_first = firstrunnerid;
		int bef_second = secondrunnerid;
		int bef_third = thirdrunnerid;
		int batterid = 0;
		int pitcherid = 0;
		
		resetRunners();
		
		if (inningState.equals("Top")) {
			batterid = awayhitterid;
			pitcherid = homepitcherid;
		}
		else if (inningState.equals("Bottom")) {
			batterid = homehitterid;
			pitcherid = awaypitcherid;
		}
		
		Logger.walk(inningState + " " + inning, batterid, pitcherid);
		
		// put the batter on second
		firstrunnerid = batterid;
		
		// advance the runner on first if he exists
		if (bef_first != 0) {
			secondrunnerid = bef_first;
			
			// advance the second runner if runner behind him
			if ( (bef_second != 0) ) {
				thirdrunnerid = bef_second;
				
				// score the runner on third if he exists and has runners behind him
				if (bef_third != 0) {
					run();
					Logger.score(bef_third);
				}
			}
		}
		else if (bef_second != 0) {
			secondrunnerid = bef_second;
			thirdrunnerid = bef_third;
		}
		else if (bef_third != 0) {
			thirdrunnerid = bef_third;
		}
	}
	
	public void resetRunners() {
		firstrunnerid = 0;
		secondrunnerid = 0;
		thirdrunnerid = 0;
	}
	public void resetCount(){
			balls = 0;
			strikes = 0;
	}
	
	public int getInning() {
		return inning;
	}

	public void setInning(int inning) {
		this.inning = inning;
	}

	public String getInningState() {
		return inningState;
	}

	public void setInningState(String inningState) {
		this.inningState = inningState;
	}

	public int getGamestatsid() {
		return gamestatsid;
	}

	public void setGamestatsid(int gamestatsid) {
		this.gamestatsid = gamestatsid;
	}

	public int getAwayscore() {
		return awayscore;
	}

	public void setAwayscore(int awayscore) {
		this.awayscore = awayscore;
	}

	public int getHomescore() {
		return homescore;
	}

	public void setHomescore(int homescore) {
		this.homescore = homescore;
	}

	public int getAwayteamid() {
		return awayteamid;
	}

	public void setAwayteamid(int awayteamid) {
		this.awayteamid = awayteamid;
	}

	public int getHometeamid() {
		return hometeamid;
	}

	public void setHometeamid(int hometeamid) {
		this.hometeamid = hometeamid;
	}

	public int getHitterid() {
		return hitterid;
	}

	public void setHitterid(int hitterid) {
		this.hitterid = hitterid;
	}

	public int getHomePitcherid() {
		return homepitcherid;
	}

	public void setHomePitcherid(int homepitcherid) {
		this.homepitcherid = homepitcherid;
	}
	
	public int getAwayPitcherid() {
		return awaypitcherid;
	}
	
	public void setAwayPitcherid(int awaypitcherid) {
		this.awaypitcherid = awaypitcherid;
	}

	public int getGameid() {
		return gameid;
	}

	public void setGameid(int gameid) {
		this.gameid = gameid;
	}

	public int getStrikes() {
		return strikes;
	}

	public void setStrikes(int strikes) {
		this.strikes = strikes;
	}

	public int getBalls() {
		return balls;
	}

	public void setBalls(int balls) {
		this.balls = balls;
	}

	public int getOuts() {
		return outs;
	}

	public void setOuts(int outs) {
		this.outs = outs;
	}

	public int getHittingteamid() {
		return hittingteamid;
	}

	public void setHittingteamid(int hittingteamid) {
		this.hittingteamid = hittingteamid;
	}

	public int getAwayhitterid() {
		return awayhitterid;
	}

	public void setAwayhitterid(int awayhitterid) {
		this.awayhitterid = awayhitterid;
	}

	public int getHomehitterid() {
		return homehitterid;
	}

	public void setHomehitterid(int homehitterid) {
		this.homehitterid = homehitterid;
	}
	
	public int getFirstrunnerid() {
		return firstrunnerid;
	}

	public void setFirstrunnerid(int firstrunnerid) {
		this.firstrunnerid = firstrunnerid;
	}

	public int getSecondrunnerid() {
		return secondrunnerid;
	}

	public void setSecondrunnerid(int secondrunnerid) {
		this.secondrunnerid = secondrunnerid;
	}

	public int getThirdrunnerid() {
		return thirdrunnerid;
	}

	public void setThirdrunnerid(int thirdrunnerid) {
		this.thirdrunnerid = thirdrunnerid;
	}

}