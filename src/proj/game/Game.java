package proj.game;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import proj.database.MySQLConnection;

import com.mysql.jdbc.Connection;

public class Game {
	
	private int id;
	private int awayid;
	private int homeid;
	private int statusid;
	
	public Game(int id) {
		try {
            Connection conn = (Connection) MySQLConnection.getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM GAME WHERE game_id = " + id + ";";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                this.id = initiateRs.getInt("game_id");
                this.awayid = initiateRs.getInt("game_awayteamid");
                this.homeid = initiateRs.getInt("game_hometeamid");
                this.statusid = initiateRs.getInt("game_statusid");
            }

            initiateRs.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public boolean isGame() {
		if (statusid == 1) {
			return true;
		}
		else 
			return false;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAwayid() {
		return awayid;
	}

	public void setAwayid(int awayid) {
		this.awayid = awayid;
	}

	public int getHomeid() {
		return homeid;
	}

	public void setHomeid(int homeid) {
		this.homeid = homeid;
	}

	public int getStatusid() {
		return statusid;
	}

	public void setStatusid(int statusid) {
		this.statusid = statusid;
	}

}
