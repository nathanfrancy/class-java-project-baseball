package proj.team;

import proj.player.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

import proj.database.MySQLConnection;

public class Team {

	private int id;
	private String location;
	private String name;
	private ArrayList<Player> team;
	
	public Team (int id) {
		try {
            Connection conn = (Connection) MySQLConnection.getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM TEAM WHERE team_id = " + id + " ORDER BY team_id desc;";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                this.id = initiateRs.getInt("team_id");
                this.location = initiateRs.getString("team_location");
                this.name = initiateRs.getString("team_name");
            }
            this.team = MySQLConnection.getTeam(this.id);

            initiateRs.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public String getPlayerList(int currentbatter, boolean current) {
		String s = "<br /><br /><table class='battingorder table table-condensed table-striped'><tr>"
				+ "<td>No.</td>"
				+ "<td>Jersey</td>"
				+ "<td>P.</td>"
				+ "<td>Name</td>"
				+ "</tr>";
		
		DecimalFormat df = new DecimalFormat("#0.000");
		
		Player currentBatter = new Player(currentbatter);
		int look = currentBatter.getBatorder();
		int count = 1;
		
		// build the batting order string
		for (Player p : team) {
			if (count == look) {
				if (current) {
					s += "<tr class='success bold'>";
				}
				else {
					s += "<tr class='warning'>";
				}
			}
			else {
				s += "<tr>";
			}
			
			s += "<td>"+ p.getBatorder() +"</td>"
					+ "<td>"+ p.getJerseyno() +"</td>"
					+ "<td>"+ p.getPosition() +"</td>"
					+ "<td>" + p.getFname() + " " + p.getLname() + "</td>"
					+ "</tr>";
			count++;
		}
		
		s+="</table>";
		
		return s;
	}
	
	public int getFirstBatter() {
		return MySQLConnection.getFirstBatter(this.id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
