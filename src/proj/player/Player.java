package proj.player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import proj.database.MySQLConnection;
import com.mysql.jdbc.Connection;

public class Player {

	private int id;
	private String fname;
	private String lname;
	private String position;
	private int batorder;
	private int jerseyno;
	private int teamid;
	private double battingaverage;
	private int homeruns;
	private int hits;
	private int atbats;
	private int pitcher;
	private String shortname;
	
	public Player (int id) {
		try {
            Connection conn = (Connection) MySQLConnection.getConnection();

            Statement initiateStmt = conn.createStatement();
            String initiateString = "SELECT * FROM PLAYER WHERE player_id = " + id + ";";
            ResultSet initiateRs = initiateStmt.executeQuery(initiateString);

            while (initiateRs.next()) {
                this.id = initiateRs.getInt("player_id");
                this.fname = initiateRs.getString("player_fname");
                this.lname = initiateRs.getString("player_lname");
                this.position = initiateRs.getString("player_position");
                this.batorder = initiateRs.getInt("player_batorder");
                this.jerseyno = initiateRs.getInt("player_jerseyno");
                this.teamid = initiateRs.getInt("player_teamid");
                this.battingaverage = initiateRs.getDouble("player_battingaverage");
                this.homeruns = initiateRs.getInt("player_homeruns");
                this.hits = initiateRs.getInt("player_hits");
                this.atbats = initiateRs.getInt("player_atbats");
                this.pitcher = initiateRs.getInt("player_atbats");
                this.shortname = this.fname.substring(0, 1) + ". " + this.lname;
            }

            initiateRs.close();
            conn.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}
	
	public String getShortName() {
		return shortname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getBatorder() {
		return batorder;
	}

	public void setBatorder(int batorder) {
		this.batorder = batorder;
	}

	public int getJerseyno() {
		return jerseyno;
	}

	public void setJerseyno(int jerseyno) {
		this.jerseyno = jerseyno;
	}

	public int getTeamid() {
		return teamid;
	}

	public void setTeamid(int teamid) {
		this.teamid = teamid;
	}

	public double getBattingaverage() {
		return battingaverage;
	}

	public void setBattingaverage(double battingaverage) {
		this.battingaverage = battingaverage;
	}

	public int getHomeruns() {
		return homeruns;
	}

	public void setHomeruns(int homeruns) {
		this.homeruns = homeruns;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public int getAtbats() {
		return atbats;
	}

	public void setAtbats(int atbats) {
		this.atbats = atbats;
	}

	public int getPitcher() {
		return pitcher;
	}

	public void setPitcher(int pitcher) {
		this.pitcher = pitcher;
	}
	
}