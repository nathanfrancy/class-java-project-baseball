<%@page import="proj.team.Team" %>
<%@page import="servlets.Init" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%!
	String teams = Init.getTeams();
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Baseball Project: By Nathan Francy, Justin Connors & James Michaelis</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="container">
	<h1 style="text-align: center;">Live In-Game Stat System</h1>
	<h3 style="text-align: center;"><em>Please select your teams to begin a game.</em></h3>
	<br />
	
	<form action="makegame" method="POST">
	
	<div class="row">
		<div class="col-md-3"></div>
		  <div class="col-md-3">
	  		<div>
				<select class="select home-box" name="home-box">
		  			<option value="0">Choose Home Team</option>
		  			<%= teams %>
	  			</select>
	  			
	  			<div class="picture home-picture">
	  				<img class="img" id="pic"src="graphics/logos/select.png" >
	  			</div>
	  		</div>
	  			
	  		</div>
		  	<div class="col-md-3">
				<select class="select away-box" name="away-box">
					<option value="0">Choose Away Team</option>
					<%= teams %>
	  			</select>
	  			
	  			<div class="picture away-picture">
	  				<img class="img" id="pic" src="graphics/logos/select.png" >
	  			</div>
	  	</div>
	  	<div class="col-md-3"></div>
	</div>
	
	<div class="button">
		<button id="play" class="btn btn-lg btn-default" type="submit">Play Ball!</button>
		<br /><br /><br />
		<em>This is a project done for our Advanced Java class (Spring 2014 semester) by Nathan Francy, Justin Connors and James Michaellis.</em>
	</div>
	
	</form>
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
var play = document.getElementById("play");
play.disabled = true;

$(".home-box").change(function() {
	var id = $(".home-box option:selected").attr("value");
	$(".home-picture img").attr("src", "graphics/logos/" + id + ".png");
	validate();
});

$(".away-box").change(function() {
	var id = $(".away-box option:selected").attr("value");
	$(".away-picture img").attr("src", "graphics/logos/" + id + ".png");
	validate();
});

function validate() {
	var home = $( "select.home-box option:selected").val();
	var away = $( "select.away-box option:selected").val();
	
	if ( (home != away) && (home != 0) && (away != 0) ) {
		play.disabled = false;
	}
	else
		play.disabled = true;
}

</script>
</body>
</html>