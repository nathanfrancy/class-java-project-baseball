<%@page import="proj.game.*" %>
<%@page import="proj.player.Player" %>
<%@page import="proj.team.Team" %>
<%@page import="servlets.Init" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%
String req_gameid = request.getParameter("gameid");
boolean isNull = true;
boolean isGame = false;
Team awayTeam = null, homeTeam = null;
Game g = null;
GameStats gs = null;

if ( req_gameid != null && (!req_gameid.equals(""))) {
	isNull = false;
	
	int requested_gameid = Integer.parseInt(request.getParameter("gameid"));
	g = new Game(requested_gameid);
	
	// checks if this is an actual game from the database
	isGame = g.isGame();
	
	awayTeam = new Team(g.getAwayid());
    homeTeam = new Team(g.getHomeid());
    gs = new GameStats(g.getId());
	
	if (isGame) {
	    isGame = true;
	}
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>In-Game Event Logger</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">Baseball Project</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Live In-Game Matchup</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="starter-template">
		<% 	if (isNull == false) { 
			Player batter = null;
			Player pitcher = null;
			Player first = null;
			Player second = null;
			Player third = null;
			boolean awayBool = false;
			boolean homeBool = false;
			
			if (gs.getInningState().equals("Top")) {
				awayBool = true;
				homeBool = false;
			}
			else {
				awayBool = false;
				homeBool = true;
			}
			
				if (isGame == false) {
					out.println("<div class='alert alert-danger'>Looks like this isn't a current game.  <a href='index.jsp' class='btn btn-danger'>Create a new game</a></div>");
				}
		%>
		
		<div class="row">
			<div class="col-sm-5">
				<h2><%= gs.getHomescore() %></h2>
				<h2><%= homeTeam.getLocation() + " " + homeTeam.getName() %></h2>
				<img src="graphics/logos/<%= homeTeam.getId() %>.png" class="ingame-logo" />
				<br />
				<em>Home Team</em>
				
				<%= homeTeam.getPlayerList(gs.getHomehitterid(), homeBool) %>
			</div>
			<div class="col-sm-2">
				<%
				if (isGame) {
					
						// initiate the batter and pitcher if top of the inning
						// away team batting, home team pitching
						if (gs.getInningState().equals("Top")) {
							batter = new Player(gs.getAwayhitterid());
							pitcher = new Player(gs.getHomePitcherid());
						}
						
						// initiate the batter and pitcher if bottom of the inning
						// away team pitching, home team batting
						else if (gs.getInningState().equals("Bottom")) {
							batter = new Player(gs.getHomehitterid());
							pitcher = new Player(gs.getAwayPitcherid());
						}
						
						// make player baserunners if they exist
						if (gs.getFirstrunnerid() != 0) {
							first = new Player(gs.getFirstrunnerid());
						}
						if (gs.getSecondrunnerid() != 0) {
							second = new Player(gs.getSecondrunnerid());
						}
						if (gs.getThirdrunnerid() != 0) {
							third = new Player(gs.getThirdrunnerid());
						}
						
					 %>
					 
					<h3>
						<%= gs.getInningState() %>
						<% if (gs.getInning() != 0) { out.println(gs.getInning()); } %>
					</h3>
					
					<strong>Current Matchup:</strong>
					<br />
					
					<h4>
					<span class="label label-warning">
						P. <%= pitcher.getFname() + " " + pitcher.getLname() %>
					</span>
					</h4>
					
					<br />
					vs.
					<br />
					
					<h4>
					<span class="label label-success">
						<%= batter.getPosition() + " " + batter.getFname() + " " + batter.getLname() %>
					</span>
					</h4>
					
					<br /><br />
					
					<strong>Count: </strong><br />
					<%=
						gs.getBalls() + "-" + gs.getStrikes() 
					%>
					<br />
					<strong>Outs: </strong><br />
					<%=
						gs.getOuts()
					%>
					
					<br />
					<br />
					<!-- current baserunners -->
					<strong>1B: </strong> 
						<%
							if (first != null) {
								out.println("<span class='label label-success'>" + first.getShortName() + "</span>");
							}
							else {
								out.println("N/A");
							}
						%>
					<br />
					
					<strong>2B: </strong> 
						<%
							if (second != null) {
								out.println("<span class='label label-success'>" + second.getShortName() + "</span>");
							}
							else {
								out.println("N/A");
							}
						%>
					<br />
					
					<strong>3B: </strong> 
						<%
							if (third != null) {
								out.println("<span class='label label-success'>" + third.getShortName() + "</span>");
							}
							else {
								out.println("N/A");
							}
						%>
						
					<br />
					
					<br />
					
					
					<h3>Events</h3>
					
					<strong>Plate: </strong>
					
					<form action="Strike" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Strike</button>
					</form>
					
					<form action="Ball" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Ball</button>
					</form>
					
					<br />
					
					<strong>Hit: </strong>
					
					<form action="Single" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Single</button>
					</form>
					
					<form action="Double" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Double</button>
					</form>
					
					<form action="Triple" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Triple</button>
					</form>
					
					<form action="HitOut" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Hit Out</button>
					</form>
					
					<br />
					
					<form action="HomeRun" method="post">
						<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
						<button type="submit" class="btn btn-default btn-xs">Home run</button>
					</form>
					
					<!-- 
					<br /><br /><br />
					
					<form action="closeGame" method="POST">
			        	<input type="hidden" name="gamestatsid" value="<%= gs.getId() %>">
			            <button class="btn btn-danger btn-xs" type="submit">Close Game</button>
			        </form>
			         -->
		        
		        <% } // if it is a game %>
				
			</div>
			<div class="col-sm-5">
				<h2><%= gs.getAwayscore() %></h2>
				<h2><%= awayTeam.getLocation() + " " + awayTeam.getName() %></h2>
				<img src="graphics/logos/<%= awayTeam.getId() %>.png" class="ingame-logo" />
				<br />
				<em>Away Team</em>
				
				<%= awayTeam.getPlayerList(gs.getAwayhitterid(), awayBool) %>
			</div>
		</div>
		
		<%
			}
			else {
				out.println("<h4>This isn't a game.</h4>");
			}
		%>
		
      </div>

    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>