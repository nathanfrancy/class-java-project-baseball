-- phpMyAdmin SQL Dump
-- version 4.1.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 29, 2014 at 11:25 PM
-- Server version: 5.6.15
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baseball_java`
--

-- --------------------------------------------------------

--
-- Table structure for table `GAME`
--

CREATE TABLE IF NOT EXISTS `GAME` (
  `game_id` int(10) NOT NULL AUTO_INCREMENT,
  `game_awayteamid` int(10) NOT NULL,
  `game_hometeamid` int(10) NOT NULL,
  `game_statusid` int(10) NOT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `GAME`
--

INSERT INTO `GAME` (`game_id`, `game_awayteamid`, `game_hometeamid`, `game_statusid`) VALUES
(10, 6, 7, 1),
(11, 1, 2, 1),
(12, 7, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GAMESTATS`
--

CREATE TABLE IF NOT EXISTS `GAMESTATS` (
  `gamestats_id` int(11) NOT NULL AUTO_INCREMENT,
  `gamestats_gameid` int(11) NOT NULL,
  `gamestats_inning` int(11) NOT NULL,
  `gamestats_inningstate` varchar(10) NOT NULL,
  `gamestats_awayteamid` int(11) NOT NULL,
  `gamestats_hometeamid` int(11) NOT NULL,
  `gamestats_awayscore` int(11) NOT NULL,
  `gamestats_homescore` int(11) NOT NULL,
  `gamestats_hitterid` int(11) NOT NULL,
  `gamestats_pitcherid` int(11) NOT NULL,
  `gamestats_strikes` int(11) NOT NULL,
  `gamestats_balls` int(11) NOT NULL,
  `gamestats_outs` int(11) NOT NULL,
  PRIMARY KEY (`gamestats_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `GAMESTATS`
--

INSERT INTO `GAMESTATS` (`gamestats_id`, `gamestats_gameid`, `gamestats_inning`, `gamestats_inningstate`, `gamestats_awayteamid`, `gamestats_hometeamid`, `gamestats_awayscore`, `gamestats_homescore`, `gamestats_hitterid`, `gamestats_pitcherid`, `gamestats_strikes`, `gamestats_balls`, `gamestats_outs`) VALUES
(8, 10, 2, 'Bottom', 6, 7, 0, 0, 1, 20, 2, 1, 2),
(9, 11, 2, 'Top', 1, 2, 0, 0, 1, 20, 2, 0, 0),
(10, 12, 1, 'Top', 7, 5, 0, 0, 1, 20, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PLAYER`
--

CREATE TABLE IF NOT EXISTS `PLAYER` (
  `player_id` int(5) NOT NULL AUTO_INCREMENT,
  `player_fname` varchar(30) NOT NULL,
  `player_lname` varchar(30) NOT NULL,
  `player_position` varchar(10) NOT NULL,
  `player_batorder` int(5) NOT NULL,
  `player_jerseyno` int(5) NOT NULL,
  `player_teamid` int(5) NOT NULL,
  `player_battingaverage` float NOT NULL,
  `player_homeruns` int(5) NOT NULL,
  `player_hits` int(5) NOT NULL,
  `player_atbats` int(5) NOT NULL,
  `player_pitcher` int(5) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `PLAYER`
--

INSERT INTO `PLAYER` (`player_id`, `player_fname`, `player_lname`, `player_position`, `player_batorder`, `player_jerseyno`, `player_teamid`, `player_battingaverage`, `player_homeruns`, `player_hits`, `player_atbats`, `player_pitcher`) VALUES
(1, 'Coco', 'Crisp', 'CF', 1, 4, 3, 0, 0, 0, 0, 0),
(2, 'John', 'Jaso', 'C', 2, 5, 3, 0, 0, 0, 0, 0),
(3, 'Josh', 'Donaldson', '3B', 3, 20, 3, 0, 0, 0, 0, 0),
(4, 'Brandon', 'Moss', 'DH', 4, 37, 3, 0, 0, 0, 0, 0),
(5, 'Yoenis', 'Cedpedes', 'LF', 5, 52, 3, 0, 0, 0, 0, 0),
(6, 'Alberto', 'Callaspo', '1B', 6, 18, 3, 0, 0, 0, 0, 0),
(7, 'Josh', 'Reddick', 'RF', 7, 16, 3, 0, 0, 0, 0, 0),
(8, 'Nick', 'Punto', 'SS', 8, 1, 3, 0, 0, 0, 0, 0),
(9, 'Eric', 'Sogard', '2B', 9, 28, 3, 0, 0, 0, 0, 0),
(10, 'Scott', 'Kazmir', 'P', 0, 26, 3, 0, 0, 0, 0, 0),
(11, 'Ben', 'Zobrist', 'LF', 1, 18, 5, 0, 0, 0, 0, 0),
(12, 'Desmond', 'Jennings', 'CF', 2, 8, 5, 0, 0, 0, 0, 0),
(13, 'Evan', 'Longoria', '3B', 3, 3, 5, 0, 0, 0, 0, 0),
(14, 'Wil', 'Myers', 'RF', 4, 9, 5, 0, 0, 0, 0, 0),
(15, 'Logan', 'Forsythe', '2B', 5, 10, 5, 0, 0, 0, 0, 0),
(16, 'Yunel', 'Escobar', 'SS', 6, 11, 5, 0, 0, 0, 0, 0),
(17, 'Ryan', 'Hanigan', 'C', 7, 24, 5, 0, 0, 0, 0, 0),
(18, 'Sean', 'Rodriguez', '1B', 8, 1, 5, 0, 0, 0, 0, 0),
(19, 'Heath', 'Bell', 'P', 0, 13, 5, 0, 0, 0, 0, 0),
(20, 'David', 'Dejesus', 'DH', 9, 7, 5, 0, 0, 0, 0, 0),
(21, 'Dustin', 'Pedroia', '2B', 1, 15, 2, 0, 0, 0, 0, 0),
(22, 'Daniel', 'Nava', 'CF', 2, 29, 2, 0, 0, 0, 0, 0),
(23, 'David ', 'Ortiz', 'DH', 3, 34, 2, 0, 0, 0, 0, 0),
(24, 'Mike ', 'Napoli', '1B', 4, 12, 2, 0, 0, 0, 0, 0),
(25, 'Xander ', 'Bogaerts', 'SS', 5, 2, 2, 0, 0, 0, 0, 0),
(26, 'Jonny ', 'Gomes', 'LF', 6, 5, 2, 0, 0, 0, 0, 0),
(27, 'A.J.', 'Pierznski', 'C', 7, 40, 2, 0, 0, 0, 0, 0),
(28, 'Jonathan', 'Herrera', '3B', 8, 10, 2, 0, 0, 0, 0, 0),
(29, 'Shane', 'Victorino', 'RF', 9, 18, 2, 0, 0, 0, 0, 0),
(30, 'Jon', 'Lester', 'P', 0, 31, 2, 0, 0, 0, 0, 1),
(31, 'J.B.', 'Shuck', 'LF', 1, 3, 6, 0, 0, 0, 0, 0),
(32, 'Mike', 'Trout', 'CF', 2, 27, 6, 0, 0, 0, 0, 0),
(33, 'Albert', 'Pujols', '1B', 3, 5, 6, 0, 0, 0, 0, 0),
(34, 'Raul', 'Ibanez', 'DH', 4, 28, 6, 0, 0, 0, 0, 0),
(35, 'Kole', 'Calhoun', 'RF', 5, 56, 6, 0, 0, 0, 0, 0),
(36, 'Howie', 'Kendrick', '2B', 6, 47, 6, 0, 0, 0, 0, 0),
(37, 'Ian', 'Stewart', '3B', 7, 44, 6, 0, 0, 0, 0, 0),
(38, 'Hank', 'Conger', 'C', 8, 16, 6, 0, 0, 0, 0, 0),
(39, 'Erick', 'Aybar', 'SS', 9, 2, 6, 0, 0, 0, 0, 0),
(40, 'C.J.', 'Wilson', 'P', 0, 33, 6, 0, 0, 0, 0, 1),
(41, 'Salvador ', 'Perez', 'C', 6, 13, 1, 0, 0, 0, 0, 0),
(42, 'Yordano', 'Ventura', 'P', 0, 30, 1, 0, 0, 0, 0, 1),
(43, 'Alchides', 'Escobar', 'SS', 9, 2, 1, 0, 0, 0, 0, 0),
(44, 'Eric', 'Hosmer', '1B', 3, 2, 1, 0, 0, 0, 0, 0),
(45, 'Omar', 'Infante', '2B', 2, 14, 1, 0, 0, 0, 0, 0),
(46, 'Mike', 'Moustakas', '3B', 7, 8, 1, 0, 0, 0, 0, 0),
(47, 'Norichika', 'Aoki', 'RF', 1, 23, 1, 0, 0, 0, 0, 0),
(48, 'Lorenzo', 'Cain', 'CF', 8, 6, 1, 0, 0, 0, 0, 0),
(49, 'Alex', 'Gordon', 'LF', 5, 4, 1, 0, 0, 0, 0, 0),
(50, 'Billy', 'Butler', 'DH', 4, 16, 1, 0, 0, 0, 0, 0),
(51, 'Alex', 'Avila', 'C', 6, 13, 4, 0, 0, 0, 0, 0),
(52, 'Miguel', 'Cabrera', '1B', 3, 24, 4, 0, 0, 0, 0, 0),
(53, 'Victor ', 'Martinez', 'DH', 4, 41, 4, 0, 0, 0, 0, 0),
(54, 'Austin ', 'Jackson', 'CF', 5, 14, 4, 0, 0, 0, 0, 0),
(55, 'Nick', 'Castellanos', '3B', 7, 9, 4, 0, 0, 0, 0, 0),
(56, 'Don', 'Kelly', 'LF', 8, 32, 4, 0, 0, 0, 0, 0),
(57, 'Tori', 'Hunter', 'RF', 2, 48, 4, 0, 0, 0, 0, 0),
(58, 'Andrew', 'Romine', 'SS', 9, 27, 4, 0, 0, 0, 0, 0),
(59, 'Justin', 'Verlander', 'P', 0, 35, 4, 0, 0, 0, 0, 1),
(60, 'Ian', 'Kinsler', '2B', 1, 3, 4, 0, 0, 0, 0, 0),
(61, 'Jose', 'Reyes', 'SS', 1, 7, 7, 0, 0, 0, 0, 0),
(62, 'Melky', 'Cabrera', 'LF', 2, 53, 7, 0, 0, 0, 0, 0),
(63, 'Jose', 'Bautista', 'CF', 3, 19, 7, 0, 0, 0, 0, 0),
(64, 'Edwin', 'Encarnacion', '1B', 4, 10, 7, 0, 0, 0, 0, 0),
(65, 'Brett', 'Lawrie', '3B', 5, 13, 7, 0, 0, 0, 0, 0),
(66, 'Juan', 'Francisco', 'DH', 6, 47, 7, 0, 0, 0, 0, 0),
(67, 'Moises', 'Sierra', 'RF', 7, 14, 7, 0, 0, 0, 0, 0),
(68, 'Josh', 'Thole', 'C', 8, 22, 7, 0, 0, 0, 0, 0),
(69, 'Jonathan', 'Diaz', '2B', 9, 1, 7, 0, 0, 0, 0, 0),
(70, 'Michael', 'Choise', 'LF', 1, 15, 8, 0, 0, 0, 0, 0),
(71, 'Elvis', 'Andrus', 'SS', 2, 1, 8, 0, 0, 0, 0, 0),
(72, 'Prince', 'Fielder', '1B', 3, 84, 8, 0, 0, 0, 0, 0),
(73, 'Adrian', 'Beltre', '3B', 4, 29, 8, 0, 0, 0, 0, 0),
(74, 'Alex', 'Rios', 'RF', 5, 51, 8, 0, 0, 0, 0, 0),
(75, 'Mitch', 'Moreland', 'DH', 6, 18, 8, 0, 0, 0, 0, 0),
(76, 'Robinson', 'Chirinos', 'C', 7, 61, 8, 0, 0, 0, 0, 0),
(77, 'Leonys', 'Martin', 'CF', 8, 2, 8, 0, 0, 0, 0, 0),
(78, 'Josh', 'Wilson', '2B', 9, 12, 8, 0, 0, 0, 0, 0),
(79, 'Yu', 'Darvish', 'P', 0, 11, 8, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `TEAM`
--

CREATE TABLE IF NOT EXISTS `TEAM` (
  `team_id` int(10) NOT NULL AUTO_INCREMENT,
  `team_location` varchar(30) NOT NULL,
  `team_name` varchar(30) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `TEAM`
--

INSERT INTO `TEAM` (`team_id`, `team_location`, `team_name`) VALUES
(1, 'Kansas City', 'Royals'),
(2, 'Boston', 'Red Sox'),
(3, 'Oakland', 'Athletics'),
(4, 'Detroit', 'Tigers'),
(5, 'Tampa Bay', 'Rays'),
(6, 'Los Angeles', 'Angels'),
(7, 'Toronto', 'Blue Jays'),
(8, 'Texas', 'Rangers');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
